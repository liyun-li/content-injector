# Content Injector

Inject custom content into page.

## Usage

Simply bookmark the content of `output.txt`.

## Contribution

Node 12+ and NPM 6+ are the only true dependencies so install them first.

To install the rest:

```sh
npm install
```

To update `output.txt`:

```sh
npm start
```

## Acknowledgement

* [Minify](https://github.com/coderaiser/minify)
* [W3 CSS](https://www.w3schools.com/w3css)
