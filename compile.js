const fs = require('fs')
const minify = require('minify')

// escape backticks
const swap = (s, from, to) => s.replace(from, '`' + to.replace(/`/g, '\`') + '`')

const main = async () => {
  const main = await minify('main.js')
  const replacements = JSON.parse(fs.readFileSync('replacements.json'))

  let injector = `(()=>{${main}})()`

  for (const key in replacements) {
    const r = await minify(replacements[key])
    injector = swap(injector, key, r)
  }

  fs.writeFileSync('output.txt', `javascript:${injector}\n`)

  console.log('OK!')
}

main()
