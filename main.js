/**
 * Hierarchy:
 * - w3css
 * - mycss
 * - overlay
 *   - content
 *     - title
 *     - codeArea
 *     - divider
 *     - actionArea
 *       - save
 *       - exportCode
 *       - clear
 *       - close
 * - edit
 */

if (window.contectInjectorByLiyun)
  for (const element of window.contectInjectorByLiyun)
    element.remove()

const getCode = () => JSON.parse(localStorage.getItem('code'))

const dce = e => document.createElement(e)

const appendNodes = (parent, childList) => {
  for (const e of childList) parent.append(e)
}

const addClasses = (nodes, classList) => {
  for (const c of classList)
    for (const n of nodes) n.classList.add(c)
}

const addStyle = (node, style) => {
  for (const key in style) node.style[key] = style[key]
}

const buttonReflect = () => {
  const sameCode = codeArea.value === getCode()
  undo.disabled = sameCode ? true : false
  save.disabled = sameCode ? true : false
  clear.disabled = !codeArea.value
  exportCode.disabled = !codeArea.value
}

const overlayStyle = {
  position: 'fixed',
  margin: 0,
  padding: 0,
  top: 0,
  left: 0,
  width: '100%',
  height: '100%',
  background: 'rgba(0, 0, 0, 0.5)',
  zIndex: 88888,
  display: 'none'
}

const contentStyle = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  backgroundColor: 'white',
  padding: '1vh 1vw 1vh 1vw',
  height: '75%',
  width: '75%'
}

const editStyle = {
  position: 'fixed',
  bottom: '1vh',
  right: '1vw',
  zIndex: 88888
}

const codeStyle = {
  height: '75%'
}

const w3css = dce('style')
const overlay = dce('div')
const content = dce('div')
const title = dce('h2')
const codeArea = dce('textarea')
const divider = dce('hr')
const actionArea = dce('div')
const save = dce('button')
const exportCode = dce('button')
const clear = dce('button')
const close = dce('button')
const undo = dce('button')
const run = dce('button')
const edit = dce('button')

w3css.textContent = __W3_CSS__
title.textContent = 'CONTECT INJECTOR'
save.textContent = 'Save'
exportCode.textContent = 'Export'
clear.textContent = 'Clear'
close.textContent = 'Close'
undo.textContent = 'Undo'
run.textContent = 'Run'
edit.textContent = 'Edit'

overlay.id = 'cibl-overlay'
content.id = 'cibl-root'
content.tabIndex = 1

codeArea.value = getCode()

save.disabled = true
undo.disabled = true
clear.disabled = !codeArea.value

codeArea.oninput = e => {
  e.preventDefault()
  buttonReflect()
}

codeArea.onkeydown = e => {
  if (e.keyCode === 9) {
    e.preventDefault()
    e.target.value += '  '
  }
}

exportCode.onclick = () => {
  if (save.disabled || !save.disabled && confirm('You must save the code first. Save now?')) {
    const a = dce('a')
    a.download = `${document.domain}.bookmarklet`
    a.href = `data:text/plain,${JSON.stringify(codeArea)}`
    save.click()
    a.click()
  }
}

save.onclick = () => {
  localStorage.setItem('code', JSON.stringify(codeArea.value))
  buttonReflect()
}

clear.onclick = () => {
  codeArea.value = ''
  clear.disabled = true
  buttonReflect()
}

close.onclick = () => {
  overlay.style.display = 'none'
  edit.style.display = ''
}

undo.onclick = () => {
  codeArea.value = getCode()
  buttonReflect()
}

run.onclick = () => eval(`(() => {${codeArea.value}})()`)

edit.onclick = () => {
  overlay.style.display = ''
  edit.style.display = 'none'
}

addClasses([codeArea], ['w3-input', 'w3-border'])
addClasses([title, actionArea], ['w3-center'])
addClasses([save, exportCode, close, clear, undo, run], ['w3-margin-left', 'w3-margin-right'])

addStyle(overlay, overlayStyle)
addStyle(content, contentStyle)
addStyle(codeArea, codeStyle)
addStyle(edit, editStyle)

appendNodes(document.head, [w3css])
appendNodes(document.body, [overlay, edit])
appendNodes(overlay, [content])
appendNodes(content, [title, codeArea, divider, actionArea])
appendNodes(actionArea, [clear, exportCode, save, undo, close, run])

if (!codeArea.value) {
  overlay.style.display = ''
  edit.style.display = 'none'
}

window.contectInjectorByLiyun = [overlay, edit]
